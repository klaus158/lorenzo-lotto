from django.shortcuts import render
import random
# Create your views here.

def index(request):
    context = {}
    lotto = random.sample(list(range(1,50)),6)
    lotto.sort()
    context["lotto"] = lotto
    print("lotto: ", context["lotto"])
    euro5 = random.sample(list(range(1,51)),5)
    euro5.sort()
    euro2 = random.sample(list(range(1,11)),2)
    euro2.sort()
    context["euro"] = [[1,2,3,4,5], [1,2]]
    context["euro5"] = euro5
    context["euro2"] = euro2
    return render(request, "lotto/index.html", context)
